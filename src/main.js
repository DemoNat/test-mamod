import Vue from 'vue';
import Axios from 'axios';
import Vuelidate from 'vuelidate';
import App from './App.vue';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.prototype.$http = Axios;

Vue.use(Vuelidate);

new Vue({
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
